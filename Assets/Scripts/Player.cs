﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    private Vector2 startTouchPosition, endTouchPosition;
    private Vector3 startPosition, endPosition;
    private float moveTime;
    private float moveDuration = 0.1f;
    public Text livestext;
    public int lives;

    private void Start()
    {
        lives = 3;
        livestext.text = "Vidas: " + lives;
    }
    // Update is called once per frame
    private void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            startTouchPosition = Input.GetTouch(0).position;

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            endTouchPosition = Input.GetTouch(0).position;

            if ((endTouchPosition.x < startTouchPosition.x) && transform.position.x > -1.75f)
                StartCoroutine(Fly("left"));

            if ((endTouchPosition.x > startTouchPosition.x) && transform.position.x < 1.75f)
                StartCoroutine(Fly("right"));
        }
    }

    private IEnumerator Fly(string whereToFly)
    {
        switch (whereToFly)
        {
            case "left":
                moveTime = 0f;
                startPosition = transform.position;
                endPosition = new Vector3
                    (startPosition.x, transform.position.y-0.3F, transform.position.z);

                while (moveTime < moveDuration)
                {
                    moveTime += Time.deltaTime;
                    transform.position = Vector2.Lerp
                        (startPosition, endPosition, moveTime / moveDuration);
                    yield return null;
                }
                break;

            case "right":
                moveTime = 0f;
                startPosition = transform.position;
                endPosition = new Vector3
                    (startPosition.x, transform.position.y + 0.3f, transform.position.z);

                while (moveTime < moveDuration)
                {
                    moveTime += Time.deltaTime;
                    transform.position = Vector2.Lerp
                        (startPosition, endPosition, moveTime / moveDuration);
                    yield return null;
                }
                break;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy")
        {
            lives--;
            Destroy(collision.gameObject);
            livestext.text = "Vidas: " + lives;
        }
    }
}
