﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    public Text score;
    private int scorenum;
    // Start is called before the first frame update
    void Start()
    {
        scorenum = 0;
        score.text = "Score: " + scorenum;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "points")
        {
            scorenum += 10;
            Destroy(collision.gameObject);
            score.text = "Score: " + scorenum;
        }
    }
}
